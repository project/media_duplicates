<?php

namespace Drupal\media_duplicates\Form;

use Drupal\media_duplicates\MediaDuplicatesChecksumBatch;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for rebuilding permissions.
 *
 * @internal
 */
class RebuildDuplicatesForm extends ConfirmFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new RebuildDuplicatesForm form.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_duplicates_refresh_checksums';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = [];
    $media_types = $this->entityTypeManager->getStorage('media_type')->loadMultiple();

    /** @var \Drupal\media\Entity\MediaType $media_type */
    foreach ($media_types as $id => $media_type) {
      $options[$id] = $media_type->label();
    }

    $form['bundles'] = [
      '#type' => 'select',
      '#title' => $this->t('Bundles'),
      '#description' => $this->t('If no bundle is selected, all bundles will be rebuilt.'),
      '#options' => $options,
      '#multiple' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to rebuild the checksums on media?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('media_duplicates.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Rebuild checksums');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This action rebuilds all checksums on the selected media bundles, and may be a lengthy process. This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    batch_set(MediaDuplicatesChecksumBatch::tasks($form_state->getValue('bundles')));
    $this->getRequest()->query->remove('destination');
    $form_state->setRedirect('media_duplicates.report');
  }

}
