<?php

namespace Drupal\media_duplicates\Commands;

use Drupal\media_duplicates\MediaDuplicatesChecksumBatch;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Media duplicates drush commands.
 */
class MediaDuplicatesCommands extends DrushCommands {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Creates a MediaDuplicatesCommands object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct();
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * The media types available to rebuild checksums for.
   *
   * @hook interact media-duplicates:checksums:rebuild
   */
  public function interact(InputInterface $input, OutputInterface $output) {
    if (empty($input->getArgument('bundles'))) {
      $media_types = $this->entityTypeManager->getStorage('media_type')->loadMultiple();
      $choices = [
        'all' => 'All',
      ];
      foreach ($media_types as $bundle => $media_type) {
        $choices[$bundle] = $media_type->label();
      }
      $bundle_choice = $this->io()->choice(dt('Media types would you like to refresh'), $choices, 'All');
      $input->setArgument('bundles', [$bundle_choice]);
    }
  }

  /**
   * Rebuild all the checksums for media revisions.
   *
   * Useful for initial installs on existing sites or if you change the
   * algorithms, or install plugins.
   *
   * @param array $bundles
   *   List of bundles to rebuild.
   *
   * @command media-duplicates:checksums:rebuild
   *
   * @usage drush media-duplicates:checksums:rebuild all
   *   Rebuild all media checksums.
   * @usage drush media-duplicates:checksums:rebuild image video
   *   Rebuild media checksums for image and video bundles.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   *
   * @validate-module-enabled media_duplicates
   */
  public function checksumsRebuild(array $bundles) {
    if (in_array('all', $bundles)) {
      $media_types = $this->entityTypeManager->getStorage('media_type')->loadMultiple();
      $bundles = array_keys($media_types);
    }

    batch_set(MediaDuplicatesChecksumBatch::tasks($bundles));
    $batch =& batch_get();
    $batch['progressive'] = TRUE;
    drush_backend_batch_process();
  }

}
