<?php

namespace Drupal\media_duplicates\Plugin\MediaDuplicatesChecksum;

use Drupal\media_duplicates\Plugin\MediaDuplicatesChecksumBase;
use Drupal\media\Entity\Media;
use Drupal\file\FileInterface;

/**
 * File duplicates checksum.
 *
 * @MediaDuplicatesChecksum(
 *   id = "file",
 *   label = @Translation("File"),
 *   media_types = {"file", "image", "audio_file", "video_file"},
 * )
 */
class File extends MediaDuplicatesChecksumBase {

  /**
   * {@inheritdoc}
   */
  public function getChecksum(Media $media) {
    $source = $media->getSource();

    /** @var \Drupal\file\Entity\File $file */
    $file = $media->get($source->configuration['source_field'])->entity;
    return ($file instanceof FileInterface) ? $this->hashFileBase64($file->getFileUri()) : NULL;
  }

  /**
   * Get a hash of the file.
   *
   * This is a duplicate of Crypt::hashBase64() but using the file handling.
   *
   * @param string $filename
   *   The file to hash.
   *
   * @return string
   *   The hash of the file.
   */
  protected function hashFileBase64($filename) {
    $hash = base64_encode(hash_file('sha256', $filename, TRUE));
    // Modify the hash so it's safe to use in URLs.
    return str_replace(['+', '/', '='], ['-', '_', ''], $hash);
  }

  /**
   * {@inheritdoc}
   */
  public function getChecksumData(Media $media) {
    // This is not used for this implementation.
    return NULL;
  }

}
