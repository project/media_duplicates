<?php

/**
 * @file
 * Hooks related to media_duplicates API.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the definition of checksum plugins.
 *
 * @param array $providers
 *   The array of checksum plugin definitions, keyed by the machine name.
 */
function hook_media_duplicates_checksum_info_alter(array &$providers) {
  // Use an existing checksum plugin for a new file media type.
  $providers['file']['media_types'][] = 'my_unique_file';
}

/**
 * @} End of "addtogroup hooks".
 */
